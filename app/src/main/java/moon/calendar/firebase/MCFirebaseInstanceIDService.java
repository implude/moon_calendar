package moon.calendar.firebase;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class MCFirebaseInstanceIDService extends FirebaseInstanceIdService{
    private static final String TAG = "MCFirebaseInstanceIDService";

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + token);


    }

    private void sendRegistrationToServer(String token){
        // 추후추가 ^^!!
    }
}
